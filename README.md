ArtistsApp
----------
Implementation of task for [Yandex.Mobilization](https://www.yandex.ru/mobilization/).

Author: Egor Danilin


How to compile it
-------------

1) Pull this project

2) Rename `app/fabric_template.properties` to `fabric.properties` and
input correct API keys for Fabric

**or**

Delete from `app/build.gradle`
```gradle
apply plugin: 'io.fabric' // this
...
dependencies {
...
    // and this
    compile('com.crashlytics.sdk.android:crashlytics:2.5.5@aar') {
        transitive = true
    }
...
}
```
and `initFabric()` method from `AndroidApp` class

3) Rename `secret_template.properties` to `secret.properties` and input
correct path to keystore and passwords for release build

**or**

Change release signing config in `app/build.gradle`

4) Compile it

License
----
    Do What The F*ck You Want To Public License v2 (WTFPL-2.0)

    See LICENSE for more information
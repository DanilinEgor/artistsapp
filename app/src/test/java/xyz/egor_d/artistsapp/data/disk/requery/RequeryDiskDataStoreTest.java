package xyz.egor_d.artistsapp.data.disk.requery;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.RuntimeEnvironment;

import java.util.List;

import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.rx.RxSupport;
import io.requery.rx.SingleEntityStore;
import io.requery.sql.EntityDataStore;
import rx.observers.TestSubscriber;
import xyz.egor_d.artistsapp.RandomData;
import xyz.egor_d.artistsapp.TestApplicationCase;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.data.model.Models;

public class RequeryDiskDataStoreTest extends TestApplicationCase {
    SingleEntityStore<Persistable> dataStore;
    RequeryDiskDataStore requeryDiskDataStore;

    @Before
    public void setUp() throws Exception {
        dataStore = RxSupport.toReactiveStore(
                new EntityDataStore<>(
                        new DatabaseSource(
                                RuntimeEnvironment.application, Models.DEFAULT, 1
                        ).getConfiguration()
                )
        );
        requeryDiskDataStore = new RequeryDiskDataStore(RuntimeEnvironment.application, dataStore);
    }

    @Test
    public void testArtists() throws Exception {
        dataStore.delete(Artist.class).get().toSingle().subscribe();

        List<Artist> artists = RandomData.randomArtists();
        dataStore.insert(artists).subscribe();

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        requeryDiskDataStore.artists().subscribe(testSubscriber);

        testSubscriber.assertValue(artists);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }

    @Test
    public void testSave() throws Exception {
        dataStore.delete(Artist.class).get().toSingle().subscribe();

        List<Artist> artists = RandomData.randomArtists();
        requeryDiskDataStore.save(artists);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataStore
                .select(Artist.class)
                .get()
                .toObservable()
                .toList()
                .first()
                .subscribe(testSubscriber);
        testSubscriber.assertValue(artists);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }

    @Test
    public void testClear() throws Exception {
        dataStore.delete(Artist.class).get().toSingle().subscribe();

        List<Artist> artists = RandomData.randomArtists();
        dataStore.insert(artists).subscribe();

        requeryDiskDataStore.clear();

        TestSubscriber<Integer> testSubscriber = new TestSubscriber<>();
        dataStore
                .count(Artist.class)
                .get()
                .toSingle()
                .subscribe(testSubscriber);
        testSubscriber.assertValue(0);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }

    @Test
    public void testArtist() throws Exception {
        dataStore.delete(Artist.class).get().toSingle().subscribe();

        List<Artist> artists = RandomData.randomArtists(10);
        Artist artist = artists.get(3);
        dataStore.insert(artists).subscribe();

        TestSubscriber<Artist> testSubscriber = new TestSubscriber<>();

        requeryDiskDataStore
                .artist(artist.getId())
                .subscribe(testSubscriber);

        testSubscriber.assertValue(artist);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }
}
package xyz.egor_d.artistsapp.data.model;

import org.junit.Test;

import java.util.Arrays;

import xyz.egor_d.artistsapp.RandomData;
import xyz.egor_d.artistsapp.TestApplicationCase;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AbstractArtistTest extends TestApplicationCase {
    @Test
    public void testGetGenresString() throws Exception {
        Artist artist = RandomData.randomArtist();
        artist.setGenres(Arrays.asList("dance", "pop", "rap"));
        assertEquals(artist.getGenresString(), "dance, pop, rap");
    }

    @Test
    public void testGetCoverUrl() throws Exception {
        Artist artist = RandomData.randomArtist();

        artist.setCover(null);
        assertNull(artist.getSmallCoverUrl(artist.getCover()));
        assertNull(artist.getBigCoverUrl(artist.getCover()));

        Cover cover = RandomData.randomCover();
        artist.setCover(cover);
        assertEquals(artist.getSmallCoverUrl(artist.getCover()), "small_cover");
        assertEquals(artist.getBigCoverUrl(artist.getCover()), "big_cover");
    }
}
package xyz.egor_d.artistsapp.data.cloud.retrofit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;
import xyz.egor_d.artistsapp.TestApplicationCase;
import xyz.egor_d.artistsapp.data.model.Artist;

import static org.mockito.Mockito.when;

public class RetrofitCloudDataStoreTest extends TestApplicationCase {
    @Mock
    ArtistsRestApi artistsRestApi;
    @Mock
    List<Artist> list;

    @InjectMocks
    RetrofitCloudDataStore retrofitCloudDataStore;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(artistsRestApi.artists()).thenReturn(Observable.just(list));
    }

    @Test
    public void testArtists() throws Exception {
        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        retrofitCloudDataStore.artists().subscribe(testSubscriber);
        testSubscriber.assertValue(list);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }
}
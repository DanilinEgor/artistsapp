package xyz.egor_d.artistsapp.data.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;
import xyz.egor_d.artistsapp.RandomData;
import xyz.egor_d.artistsapp.TestApplicationCase;
import xyz.egor_d.artistsapp.data.cloud.CloudDataStore;
import xyz.egor_d.artistsapp.data.disk.DiskDataStore;
import xyz.egor_d.artistsapp.data.exception.NoInternetConnectionException;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.ConnectivityUtils;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DataManagerTest extends TestApplicationCase {
    @Mock
    DiskDataStore diskDataStore;
    @Mock
    CloudDataStore cloudDataStore;
    @Mock
    ConnectivityUtils utils;

    @InjectMocks
    DataManager dataManager;

    List<Artist> diskArtists, cloudArtists;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        diskArtists = RandomData.randomArtists();
        cloudArtists = RandomData.randomArtists();
        when(diskDataStore.artists()).thenReturn(Observable.just(diskArtists));
        when(cloudDataStore.artists()).thenReturn(Observable.just(cloudArtists));
    }

    @Test
    public void testArtistsExpiredWithNetwork() throws Exception {
        when(diskDataStore.isExpired()).thenReturn(true);
        when(utils.isNetworkConnected()).thenReturn(true);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artists().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(cloudArtists);
        testSubscriber.assertCompleted();

        verify(diskDataStore).isExpired();
        verify(cloudDataStore).artists();
        verify(diskDataStore, never()).artists();
    }

    @Test
    public void testArtistsExpiredWithoutNetwork() throws Exception {
        when(diskDataStore.isExpired()).thenReturn(true);
        when(utils.isNetworkConnected()).thenReturn(false);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artists().subscribe(testSubscriber);
        testSubscriber.assertError(NoInternetConnectionException.class);

        verify(diskDataStore).isExpired();
        verify(cloudDataStore, never()).artists();
        verify(diskDataStore, never()).artists();
    }

    @Test
    public void testArtistsWithNetworkWithCache() throws Exception {
        when(diskDataStore.isExpired()).thenReturn(false);
        when(utils.isNetworkConnected()).thenReturn(true);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artists().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(diskArtists);
        testSubscriber.assertCompleted();

        verify(diskDataStore).isExpired();
        verify(diskDataStore).artists();
    }

    @Test
    public void testArtistsWithNetworkWithoutCache() throws Exception {
        when(diskDataStore.isExpired()).thenReturn(false);
        when(utils.isNetworkConnected()).thenReturn(true);

        when(diskDataStore.artists()).thenReturn(Observable.empty());

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artists().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(cloudArtists);
        testSubscriber.assertCompleted();

        verify(diskDataStore).isExpired();
        verify(cloudDataStore).artists();
        verify(diskDataStore).artists();

        when(diskDataStore.artists()).thenReturn(Observable.just(diskArtists));
    }

    @Test
    public void testArtistsWithoutNetworkWithCache() throws Exception {
        when(diskDataStore.isExpired()).thenReturn(false);
        when(utils.isNetworkConnected()).thenReturn(false);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artists().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(diskArtists);
        testSubscriber.assertCompleted();

        verify(diskDataStore).isExpired();
        verify(diskDataStore).artists();
    }

    @Test
    public void testArtistsFromCloudWithNetwork() throws Exception {
        when(utils.isNetworkConnected()).thenReturn(true);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artistsFromCloud().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        testSubscriber.assertValue(cloudArtists);
        testSubscriber.assertCompleted();

        verify(cloudDataStore).artists();
    }

    @Test
    public void testArtistsFromCloudWithoutNetwork() throws Exception {
        when(utils.isNetworkConnected()).thenReturn(false);

        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artistsFromCloud().subscribe(testSubscriber);
        testSubscriber.assertError(NoInternetConnectionException.class);

        verify(cloudDataStore, never()).artists();
    }

    @Test
    public void testArtistsFromDisk() throws Exception {
        TestSubscriber<List<Artist>> testSubscriber = new TestSubscriber<>();
        dataManager.artistsFromDisk().subscribe(testSubscriber);
        testSubscriber.assertValue(diskArtists);
        testSubscriber.assertCompleted();

        verify(cloudDataStore, never()).artists();
    }

    @Test
    public void testArtist() throws Exception {
        Artist artist = RandomData.randomArtist();
        artist.setId(3);

        when(diskDataStore.artist(3)).thenReturn(Observable.just(artist));

        TestSubscriber<Artist> testSubscriber = new TestSubscriber<>();
        dataManager.artist(artist.getId()).subscribe(testSubscriber);
        testSubscriber.assertValue(artist);
        testSubscriber.assertNoErrors();
        testSubscriber.assertCompleted();
    }
}
package xyz.egor_d.artistsapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.data.model.Cover;

public class RandomData {
    public static Artist randomArtist() {
        Random random = new Random();
        String[] names = new String[]{"Alice", "Bob", "Carol"};
        String[] links = new String[]{"link1", "link2", "link3"};
        String[] descriptions = new String[]{"desc1", "desc2", "desc3"};

        Artist artist = new Artist();
        artist.setId(random.nextInt());
        artist.setName(names[random.nextInt(names.length)]);
        artist.setGenres(Arrays.asList("dance", "electronics", "pop"));
        artist.setTracks(random.nextInt(50));
        artist.setAlbums(random.nextInt(50));
        artist.setLink(links[random.nextInt(links.length)]);
        artist.setDescription(descriptions[random.nextInt(descriptions.length)]);
        artist.setCover(randomCover());

        return artist;
    }

    public static List<Artist> randomArtists() {
        return randomArtists(new Random().nextInt(10) + 1);
    }

    public static List<Artist> randomArtists(int count) {
        List<Artist> artists = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            artists.add(randomArtist());
        }
        return artists;
    }

    public static Cover randomCover() {
        Cover cover = new Cover();
        cover.setSmall("small_cover");
        cover.setBig("big_cover");
        return cover;
    }
}

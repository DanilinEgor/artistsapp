package xyz.egor_d.artistsapp.domain;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.data.repository.DataRepository;

/**
 * Implementation of {@link Interactor} for getting list of artists
 */
public class ArtistsListInteractor extends Interactor {
    private final DataRepository dataRepository;

    @Inject
    public ArtistsListInteractor(
            @Named("subscribeScheduler") final Scheduler subscribeScheduler,
            @Named("observeScheduler") final Scheduler observeScheduler,
            DataRepository dataRepository
    ) {
        super(subscribeScheduler, observeScheduler);
        this.dataRepository = dataRepository;
    }

    /**
     * Gets artists from data repository
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    public Observable<List<Artist>> artists() {
        return dataRepository
                .artists()
                .compose(applySchedulers());
    }

    /**
     * Gets artists from cloud
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    public Observable<List<Artist>> artistsFromCloud() {
        return dataRepository
                .artistsFromCloud()
                .compose(applySchedulers());
    }
}

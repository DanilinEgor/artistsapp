package xyz.egor_d.artistsapp.domain;

import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.data.repository.DataRepository;

/**
 * Implementation of {@link Interactor} for getting info about {@link Artist}
 */
public class ArtistInfoInteractor extends Interactor {
    private DataRepository dataRepository;
    private int artistId;

    public ArtistInfoInteractor(
            @Named("subscribeScheduler") final Scheduler subscribeScheduler,
            @Named("observeScheduler") final Scheduler observeScheduler,
            DataRepository dataRepository,
            int artistId
    ) {
        super(subscribeScheduler, observeScheduler);
        this.dataRepository = dataRepository;
        this.artistId = artistId;
    }

    /**
     * Gets artist info from data repository
     *
     * @return {@link rx.Observable} that emits {@link Artist}
     */
    public Observable<Artist> artist() {
        return dataRepository.artist(artistId);
    }
}

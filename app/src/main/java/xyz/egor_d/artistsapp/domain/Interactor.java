package xyz.egor_d.artistsapp.domain;

import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;

/**
 * A parent abstract class for interactors that contain business logic
 */
public abstract class Interactor {
    protected final Scheduler subscribeScheduler;
    protected final Scheduler observeScheduler;

    // transformer for subscribing all Observables to subscribeScheduler
    // and observing on observeScheduler
    protected final Observable.Transformer<Observable, Observable> schedulersTransformer;

    @SuppressWarnings("unchecked")
    protected <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }

    public Interactor(
            @Named("subscribeScheduler") Scheduler subscribeScheduler,
            @Named("observeScheduler") Scheduler observeScheduler
    ) {
        this.subscribeScheduler = subscribeScheduler;
        this.observeScheduler = observeScheduler;
        schedulersTransformer =
                observable -> observable
                        .subscribeOn(subscribeScheduler)
                        .observeOn(observeScheduler)
                        .unsubscribeOn(subscribeScheduler);
    }
}

package xyz.egor_d.artistsapp.data.cloud.retrofit;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * Retrofit service for artists API
 */
public interface ArtistsRestApi {
    @GET("artists.json")
    Observable<List<Artist>> artists();
}

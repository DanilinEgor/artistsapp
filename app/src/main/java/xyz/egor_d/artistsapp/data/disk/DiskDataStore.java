package xyz.egor_d.artistsapp.data.disk;

import java.util.List;

import rx.Observable;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * An interface representing disk data store
 */
public interface DiskDataStore {
    /**
     * Gets list of {@link Artist} from disk
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    Observable<List<Artist>> artists();

    /**
     * Gets concrete {@link Artist} with artistId
     *
     * @param artistId id for retrieving {@link Artist}
     * @return {@link rx.Observable} that emits {@link Artist}
     */
    Observable<Artist> artist(int artistId);

    /**
     * Saves given list of {@link Artist} on disk
     *
     * @param artists list for saving
     */
    void save(List<Artist> artists);

    /**
     * Checks if disk cache is expired
     *
     * @return true if cache is expired, false otherwise
     */
    boolean isExpired();

    /**
     * Clears cache
     */
    void clear();
}

package xyz.egor_d.artistsapp.data.repository;

import java.util.List;

import rx.Observable;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * An interface for representing data repository, that controls disk and cloud storages
 */
public interface DataRepository {
    /**
     * Gets list of {@link Artist} with some internal logic
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    Observable<List<Artist>> artists();

    /**
     * Simply gets list of {@link Artist} from cloud
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    Observable<List<Artist>> artistsFromCloud();

    /**
     * Simply gets list of {@link Artist} from cache
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    Observable<List<Artist>> artistsFromDisk();

    /**
     * Gets {@link Artist} for some id
     *
     * @param id id for retrieving artist
     * @return {@link rx.Observable} that emits {@link Artist}
     */
    Observable<Artist> artist(int id);
}

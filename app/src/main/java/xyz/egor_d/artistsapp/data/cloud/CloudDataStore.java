package xyz.egor_d.artistsapp.data.cloud;

import java.util.List;

import rx.Observable;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * An interface representing cloud data storage
 */
public interface CloudDataStore {
    /**
     * Gets list of {@link Artist} from cloud
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    Observable<List<Artist>> artists();
}

package xyz.egor_d.artistsapp.data.repository;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.artistsapp.data.cloud.CloudDataStore;
import xyz.egor_d.artistsapp.data.disk.DiskDataStore;
import xyz.egor_d.artistsapp.data.exception.NoInternetConnectionException;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.ConnectivityUtils;

/**
 * Implementation of {@link DataRepository}
 */
public class DataManager implements DataRepository {
    private final DiskDataStore diskDataStore;
    private final CloudDataStore cloudDataStore;
    private final ConnectivityUtils utils;

    @Inject
    public DataManager(
            DiskDataStore diskDataStore,
            CloudDataStore cloudDataStore,
            ConnectivityUtils utils
    ) {
        this.diskDataStore = diskDataStore;
        this.cloudDataStore = cloudDataStore;
        this.utils = utils;
    }

    /**
     * Tries to get data from cache;
     * if it is expired, then get data from cloud
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    @Override
    public Observable<List<Artist>> artists() {
        Observable<List<Artist>> dataObservable;

        if (diskDataStore.isExpired()) {
            dataObservable = artistsFromCloud();
        } else {
            dataObservable = artistsFromDisk().switchIfEmpty(artistsFromCloud());
        }

        return dataObservable;
    }

    /**
     * If network is connected, then return data from cloud;
     * else return {@link rx.Observable} with {@link NoInternetConnectionException}
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    @Override
    public Observable<List<Artist>> artistsFromCloud() {
        Observable<List<Artist>> cloudDataObservable;

        if (utils.isNetworkConnected()) {
            cloudDataObservable = cloudDataStore
                    .artists()
                    .doOnNext(artists -> {
                        diskDataStore.clear();
                        diskDataStore.save(artists);
                    });
        } else {
            cloudDataObservable = Observable.error(new NoInternetConnectionException());
        }

        return cloudDataObservable;
    }

    /**
     * Simply gets data from cache
     *
     * @return {@link rx.Observable} that emits list of {@link Artist}
     */
    @Override
    public Observable<List<Artist>> artistsFromDisk() {
        return diskDataStore.artists();
    }

    /**
     * This implementation simply always gets info from cache
     *
     * @param id id for retrieving artist
     * @return {@link rx.Observable} that emits {@link Artist}
     */
    @Override
    public Observable<Artist> artist(final int id) {
        return diskDataStore
                .artist(id);
    }
}

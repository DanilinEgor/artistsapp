package xyz.egor_d.artistsapp.data.exception;

/**
 * {@link Exception} for no internet connection and no disk cache
 */
public class NoInternetConnectionException extends Exception {
}

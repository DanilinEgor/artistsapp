package xyz.egor_d.artistsapp.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;

@Entity
public class AbstractCover implements Parcelable, Persistable {
    @Key
    @Generated
    int id;

    @SerializedName("small")
    String small;

    @SerializedName("big")
    String big;

    @OneToOne(mappedBy = "cover")
    Artist artist;

    public AbstractCover() {
    }

    protected AbstractCover(Parcel in) {
        id = in.readInt();
        small = in.readString();
        big = in.readString();
    }

    public static final Creator<AbstractCover> CREATOR = new Creator<AbstractCover>() {
        @Override
        public AbstractCover createFromParcel(Parcel in) {
            return new AbstractCover(in);
        }

        @Override
        public AbstractCover[] newArray(int size) {
            return new AbstractCover[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeInt(id);
        dest.writeString(small);
        dest.writeString(big);
    }
}
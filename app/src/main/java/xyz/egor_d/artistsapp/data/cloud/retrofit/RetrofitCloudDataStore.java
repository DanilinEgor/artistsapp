package xyz.egor_d.artistsapp.data.cloud.retrofit;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.artistsapp.data.cloud.CloudDataStore;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * Retrofit implementation of {@link CloudDataStore}
 */
public class RetrofitCloudDataStore implements CloudDataStore {
    private final ArtistsRestApi rest;

    @Inject
    public RetrofitCloudDataStore(ArtistsRestApi rest) {
        this.rest = rest;
    }

    @Override
    public Observable<List<Artist>> artists() {
        return rest.artists();
    }
}

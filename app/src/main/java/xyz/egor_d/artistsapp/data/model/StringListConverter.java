package xyz.egor_d.artistsapp.data.model;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import io.requery.Converter;

class StringListConverter implements Converter<List<String>, String> {

    @SuppressWarnings("unchecked")
    @Override
    public Class<List<String>> mappedType() {
        return (Class) List.class;
    }

    @Override
    public Class<String> persistedType() {
        return String.class;
    }

    @Override
    public Integer persistedSize() {
        return null;
    }

    @Override
    public List<String> convertToMapped(final Class<? extends List<String>> type, final String value) {
        ArrayList<String> list = new ArrayList<>();
        if (value != null) {
            String[] parts = value.split(",");
            for (String part : parts) {
                if (part.length() > 0) {
                    list.add(part.trim());
                }
            }
        }
        return list;
    }

    @Override
    public String convertToPersisted(List<String> value) {
        if (value == null) {
            return "";
        }
        return TextUtils.join(",", value);
    }
}

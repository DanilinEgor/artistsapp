package xyz.egor_d.artistsapp.data.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;
import xyz.egor_d.artistsapp.R;

@Entity
public class AbstractArtist implements Parcelable, Persistable {
    @Key
    @Generated
    int keyId;

    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("genres")
    @Nullable
    @Convert(StringListConverter.class)
    List<String> genres;

    @SerializedName("tracks")
    int tracks;

    @SerializedName("albums")
    int albums;

    @SerializedName("link")
    @Nullable
    String link;

    @SerializedName("description")
    @Nullable
    String description;

    @SerializedName("cover")
    @Nullable
    @OneToOne
    @ForeignKey
    Cover cover;

    public AbstractArtist() {
    }

    public String getGenresString() {
        return TextUtils.join(", ", genres);
    }

    public String getInfoString(Context context) {
        return context.getString(R.string.artist_info,
                context.getResources().getQuantityString(R.plurals.albums, albums, albums),
                context.getResources().getQuantityString(R.plurals.songs, tracks, tracks));
    }

    public String getSmallCoverUrl(final Cover cover) {
        if (cover != null) {
            return cover.getSmall();
        }
        return null;
    }

    public String getBigCoverUrl(final Cover cover) {
        if (cover != null) {
            return cover.getBig();
        }
        return null;
    }

    public String getBio() {
        StringBuilder sb = new StringBuilder().append(description);
        if (link != null) {
            sb.append("\n\nLink: ").append(link);
        }
        return sb.toString();
    }

    protected AbstractArtist(Parcel in) {
        keyId = in.readInt();
        id = in.readInt();
        name = in.readString();
        genres = in.createStringArrayList();
        tracks = in.readInt();
        albums = in.readInt();
        link = in.readString();
        description = in.readString();
        cover = in.readParcelable(AbstractCover.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(keyId);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeStringList(genres);
        dest.writeInt(tracks);
        dest.writeInt(albums);
        dest.writeString(link);
        dest.writeString(description);
        dest.writeParcelable(cover, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AbstractArtist> CREATOR = new Creator<AbstractArtist>() {
        @Override
        public AbstractArtist createFromParcel(Parcel in) {
            return new AbstractArtist(in);
        }

        @Override
        public AbstractArtist[] newArray(int size) {
            return new AbstractArtist[size];
        }
    };
}

package xyz.egor_d.artistsapp.data.disk.requery;

import android.content.Context;
import android.preference.PreferenceManager;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.requery.Persistable;
import io.requery.rx.SingleEntityStore;
import rx.Observable;
import timber.log.Timber;
import xyz.egor_d.artistsapp.data.disk.DiskDataStore;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * Implementation of {@link DiskDataStore} cache with requery
 */
public class RequeryDiskDataStore implements DiskDataStore {
    private static final String LAST_UPDATE = "last_update";

    // in this implementation cache expires after one day
    private static final long EXPIRATION_TIME = TimeUnit.DAYS.toMillis(1);

    private final Context context;
    private final SingleEntityStore<Persistable> dataStore;

    @Inject
    public RequeryDiskDataStore(Context context, SingleEntityStore<Persistable> dataStore) {
        this.context = context;
        this.dataStore = dataStore;
    }

    /**
     * Checks if no artists stored, then return empty Observable, else return list of artists
     */
    @Override
    public Observable<List<Artist>> artists() {
        return dataStore
                .count(Artist.class)
                .get()
                .toSingle()
                .toObservable()
                .flatMap(count -> {
                    if (count == 0) {
                        return Observable.empty();
                    }
                    return dataStore
                            .select(Artist.class)
                            .get()
                            .toObservable()
                            .toList()
                            .first();
                });
    }

    @Override
    public void save(final List<Artist> artists) {
        dataStore
                .insert(artists)
                .subscribe(
                        list -> setLastUpdateTime(),
                        throwable -> {
                            Timber.e(throwable, "Error");
                            clear();
                        }
                );
    }

    /**
     * If more than EXPIRATION_TIME passed, then clear cache and return true;
     * Else return false
     */
    @Override
    public boolean isExpired() {
        boolean expired = System.currentTimeMillis() - getLastUpdateTime() > EXPIRATION_TIME;
        if (expired) {
            clear();
        }
        return expired;
    }

    /**
     * Get artists last updated time
     */
    private long getLastUpdateTime() {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(LAST_UPDATE, 0);
    }

    /**
     * Save current time as artists last updated time
     */
    private void setLastUpdateTime() {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putLong(LAST_UPDATE, System.currentTimeMillis())
                .commit();
    }

    @Override
    public void clear() {
        dataStore
                .delete(Artist.class)
                .get()
                .toSingle()
                .subscribe(
                        count -> Timber.v("Deleted count=" + count),
                        throwable -> Timber.e(throwable, "Error")
                );
    }

    @Override
    public Observable<Artist> artist(final int artistId) {
        return dataStore
                .select(Artist.class)
                .where(Artist.ID.eq(artistId))
                .get()
                .toObservable();
    }
}

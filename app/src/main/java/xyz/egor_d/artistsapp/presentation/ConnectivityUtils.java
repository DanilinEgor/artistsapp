package xyz.egor_d.artistsapp.presentation;

import android.content.Context;
import android.net.ConnectivityManager;

import javax.inject.Inject;

public class ConnectivityUtils {
    private final Context context;

    @Inject
    public ConnectivityUtils(Context context) {
        this.context = context;
    }

    /**
     * Check if network connected
     */
    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}

package xyz.egor_d.artistsapp.presentation.artistslist.di;

import dagger.Component;
import xyz.egor_d.artistsapp.presentation.PerActivity;
import xyz.egor_d.artistsapp.presentation.application.di.ApplicationComponent;
import xyz.egor_d.artistsapp.presentation.artistslist.view.ArtistsListFragment;

/**
 * A component for Activity with artists list. Modules for this component must be
 * scoped with {@link PerActivity} annotation
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = ArtistsListModule.class
)
public interface ArtistsListComponent {
    void inject(ArtistsListFragment artistsListFragment);
}

package xyz.egor_d.artistsapp.presentation.artistslist.di;

import dagger.Module;

/**
 * Dagger module for {@link ArtistsListComponent}
 */
@Module
public class ArtistsListModule {
}

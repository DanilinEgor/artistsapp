package xyz.egor_d.artistsapp.presentation.base;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.presentation.application.AndroidApp;
import xyz.egor_d.artistsapp.presentation.application.di.ApplicationComponent;

/**
 * Base class for Activities
 */
public class BaseActivity extends AppCompatActivity {
    /**
     * Gets {@link ApplicationComponent}
     */
    protected ApplicationComponent getApplicationComponent() {
        return ((AndroidApp) getApplicationContext()).getApplicationComponent();
    }

    /**
     * Sets fragment for this Activity. Activity must have R.layout.activity_with_fragment layout
     */
    protected void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }
}

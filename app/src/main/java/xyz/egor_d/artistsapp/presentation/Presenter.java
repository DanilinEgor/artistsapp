package xyz.egor_d.artistsapp.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Interface representing a Presenter in a MVP
 */
public interface Presenter {
    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onCreate() method.
     */
    void onCreate();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onViewCreated() method.
     */
    void onViewCreated(final View view, @Nullable final Bundle savedInstanceState);

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroyView() method.
     */
    void onDestroyView();

    /**
     * Method that control the lifecycle of the view. It should be called in the view's
     * (Activity or Fragment) onDestroy() method.
     */
    void onDestroy();
}

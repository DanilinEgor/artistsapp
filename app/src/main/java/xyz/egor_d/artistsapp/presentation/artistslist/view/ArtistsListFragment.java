package xyz.egor_d.artistsapp.presentation.artistslist.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.artistinfo.view.ArtistInfoActivity;
import xyz.egor_d.artistsapp.presentation.artistslist.di.ArtistsListComponent;
import xyz.egor_d.artistsapp.presentation.artistslist.presenter.ArtistsListPresenter;
import xyz.egor_d.artistsapp.presentation.base.BaseFragment;

/**
 * Fragment with artists list. Implements View in MVP
 */
public class ArtistsListFragment extends BaseFragment implements ArtistsListView {
    @Inject
    ArtistsListPresenter artistsListPresenter;
    @Inject
    ArtistsAdapter artistsAdapter;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.error_view_container)
    View errorContainer;
    @Bind(R.id.error_text_view)
    TextView errorTextView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    public ArtistsListFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(ArtistsListComponent.class).inject(this);
        artistsListPresenter.onCreate();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_artists_list, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();
        setupSwipeRefreshLayout();
        return view;
    }

    private void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            artistsListPresenter.onRefresh();
        });
    }

    private void setupRecyclerView() {
        artistsAdapter.setArtistClickListener((artist, image) -> {
            artistsListPresenter.onArtistClicked(artist, image);
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(artistsAdapter);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST)
        );
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        artistsListPresenter.setArtistsListView(this);
        artistsListPresenter.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerView.setAdapter(null);
        ButterKnife.unbind(this);
        artistsListPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        artistsListPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showLoading() {
        // hack because refreshing not showing initially
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(false));
    }

    @Override
    public void showData(final List<Artist> artists) {
        errorContainer.setVisibility(View.GONE);
        artistsAdapter.setData(artists);
    }

    @Override
    public void openInfo(final Artist artist, View image) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                getActivity(),
                image,
                getString(R.string.transition_artist_image)
        );
        startActivity(ArtistInfoActivity.getOpenIntent(getActivity(), artist.getId()), options.toBundle());
    }

    /**
     * Shows toast, if recycler view has any data;
     * shows error screen, otherwise
     */
    @Override
    public void showErrorMessage(final String message) {
        if (artistsAdapter.getItemCount() > 0) {
            super.showErrorMessage(message);
        } else {
            errorTextView.setText(message);
            errorContainer.setVisibility(View.VISIBLE);
        }
    }
}

package xyz.egor_d.artistsapp.presentation.application.di;

import android.content.Context;

import com.squareup.picasso.Picasso;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;
import xyz.egor_d.artistsapp.data.repository.DataRepository;

/**
 * A component with application lifetime
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    Context context();

    Picasso picasso();

    @Named("subscribeScheduler")
    Scheduler subscribeScheduler();

    @Named("observeScheduler")
    Scheduler observeScheduler();

    DataRepository dataRepository();
}

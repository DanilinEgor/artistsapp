package xyz.egor_d.artistsapp.presentation.application.di;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.rx.RxSupport;
import io.requery.rx.SingleEntityStore;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.egor_d.artistsapp.data.cloud.CloudDataStore;
import xyz.egor_d.artistsapp.data.cloud.retrofit.ArtistsRestApi;
import xyz.egor_d.artistsapp.data.cloud.retrofit.RetrofitCloudDataStore;
import xyz.egor_d.artistsapp.data.disk.DiskDataStore;
import xyz.egor_d.artistsapp.data.disk.requery.RequeryDiskDataStore;
import xyz.egor_d.artistsapp.data.model.Models;
import xyz.egor_d.artistsapp.data.repository.DataManager;
import xyz.egor_d.artistsapp.data.repository.DataRepository;

/**
 * Dagger module for {@link ApplicationComponent}
 */
@Module
public class ApplicationModule {
    private Context context;

    public ApplicationModule(final Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    @Named("endpoint")
    public String provideEndpoint() {
        return "http://download.cdn.yandex.net/mobilization-2016/";
    }

    @Singleton
    @Provides
    public Gson provideGson() {
        Gson gson = new GsonBuilder()
                .create();
        return gson;
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                .build();
        return okHttpClient;
    }

    @Singleton
    @Provides
    public Retrofit providesRetrofit(
            @Named("endpoint") String endpoint,
            Gson gson,
            OkHttpClient okHttpClient
    ) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Singleton
    @Provides
    public ArtistsRestApi provideArtistsRest(
            Retrofit retrofit
    ) {
        return retrofit.create(ArtistsRestApi.class);
    }

    @Singleton
    @Provides
    @Named("subscribeScheduler")
    public Scheduler provideSubscribeScheduler() {
        return Schedulers.io();
    }

    @Singleton
    @Provides
    @Named("observeScheduler")
    public Scheduler provideObserveScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Singleton
    @Provides
    public DiskDataStore provideDiskDataStore(RequeryDiskDataStore requeryDiskDataStore) {
        return requeryDiskDataStore;
    }

    @Singleton
    @Provides
    public CloudDataStore provideCloudDataStore(RetrofitCloudDataStore retrofitCloudDataStore) {
        return retrofitCloudDataStore;
    }

    @Singleton
    @Provides
    public DataRepository provideDataRepository(DataManager dataManager) {
        return dataManager;
    }

    @Singleton
    @Provides
    public SingleEntityStore<Persistable> provideDataStore(Context context) {
        DatabaseSource source = new DatabaseSource(context, Models.DEFAULT, 1);
        Configuration configuration = source.getConfiguration();
        SingleEntityStore<Persistable> dataStore = RxSupport.toReactiveStore(
                new EntityDataStore<>(configuration));
        return dataStore;
    }

    @Singleton
    @Provides
    public Picasso providePicasso(Context context) {
        return Picasso.with(context);
    }
}

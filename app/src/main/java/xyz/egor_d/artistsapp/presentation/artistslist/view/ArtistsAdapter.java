package xyz.egor_d.artistsapp.presentation.artistslist.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.data.model.Artist;

/**
 * RecyclerView.Adapter for {@link ArtistsListFragment}
 */
public class ArtistsAdapter extends RecyclerView.Adapter<ArtistsAdapter.ArtistVH> {
    private List<Artist> artistsData;
    private LayoutInflater layoutInflater;
    private ArtistClickListener artistClickListener;
    private Picasso picasso;

    @Inject
    public ArtistsAdapter(
            Context context,
            Picasso picasso
    ) {
        this.picasso = picasso;
        this.layoutInflater = LayoutInflater.from(context);
        this.artistsData = Collections.emptyList();
    }

    @Override
    public ArtistVH onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = layoutInflater.inflate(R.layout.item_artist, parent, false);
        return new ArtistVH(view);
    }

    @Override
    public void onBindViewHolder(final ArtistVH holder, final int position) {
        Artist artist = artistsData.get(position);

        holder.name.setText(artist.getName());
        holder.genres.setText(artist.getGenresString());
        holder.info.setText(artist.getInfoString(holder.info.getContext()));

        // TODO: it is shit to load big cover here, but if we load small, then animation suffers
        // TODO: if no internet connection, then here will be grey pic, that not animated
        picasso.load(artist.getBigCoverUrl(artist.getCover()))
                .placeholder(R.drawable.placeholder_background)
                .error(R.drawable.placeholder_background)
                .into(holder.image);

        holder.itemView.setOnClickListener(v -> {
            if (artistClickListener != null) {
                artistClickListener.onArtistClicked(artist, holder.image);
            }
        });
    }

    @Override
    public int getItemCount() {
        return artistsData.size();
    }

    public void setData(List<Artist> data) {
        if (data == null) {
            throw new NullPointerException("Adapter data can not be null");
        }
        this.artistsData = data;
        notifyDataSetChanged();
    }

    public void setArtistClickListener(ArtistClickListener artistClickListener) {
        this.artistClickListener = artistClickListener;
    }

    /**
     * An interface-callback for clicking on some Artist item
     */
    public interface ArtistClickListener {
        /**
         * User clicked some item
         *
         * @param artist clicked artist
         */
        void onArtistClicked(Artist artist, View image);
    }

    static class ArtistVH extends RecyclerView.ViewHolder {
        @Bind(R.id.artist_name)
        public TextView name;
        @Bind(R.id.artist_genres)
        public TextView genres;
        @Bind(R.id.artist_info)
        public TextView info;
        @Bind(R.id.artist_image)
        public ImageView image;

        public ArtistVH(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

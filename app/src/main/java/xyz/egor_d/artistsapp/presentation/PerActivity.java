package xyz.egor_d.artistsapp.presentation;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * A scoping annotation for objects, that must have lifetime attached to some Activity
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}

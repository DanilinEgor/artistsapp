package xyz.egor_d.artistsapp.presentation.artistinfo.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.MenuItem;

import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.presentation.HasComponent;
import xyz.egor_d.artistsapp.presentation.artistinfo.di.ArtistInfoComponent;
import xyz.egor_d.artistsapp.presentation.artistinfo.di.ArtistInfoModule;
import xyz.egor_d.artistsapp.presentation.artistinfo.di.DaggerArtistInfoComponent;
import xyz.egor_d.artistsapp.presentation.base.BaseActivity;

/**
 * Activity with artist info. Has {@link ArtistInfoComponent}
 */
public class ArtistInfoActivity extends BaseActivity implements HasComponent<ArtistInfoComponent> {
    public static final String ARTIST_ID = "artist_id";

    private ArtistInfoComponent artistInfoComponent;
    private int artistId;

    public static Intent getOpenIntent(Context context, int id) {
        Intent intent = new Intent(context, ArtistInfoActivity.class);
        intent.putExtra(ARTIST_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_fragment);

        // Gets artistId from savedInstanceState, if this is recreating Activity;
        // or from getIntent(), if this is first time creating
        if (savedInstanceState == null) {
            artistId = getIntent().getIntExtra(ARTIST_ID, -1);
            setFragment(new ArtistInfoFragment());
        } else {
            artistId = savedInstanceState.getInt(ARTIST_ID);
        }

        buildComponent();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        if (outState != null) {
            outState.putInt(ARTIST_ID, artistId);
        }
        super.onSaveInstanceState(outState);
    }

    private void buildComponent() {
        artistInfoComponent = DaggerArtistInfoComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .artistInfoModule(new ArtistInfoModule(artistId))
                .build();
    }

    @Override
    public ArtistInfoComponent getComponent() {
        return artistInfoComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ActivityCompat.finishAfterTransition(this);
        }
        return super.onOptionsItemSelected(item);
    }
}

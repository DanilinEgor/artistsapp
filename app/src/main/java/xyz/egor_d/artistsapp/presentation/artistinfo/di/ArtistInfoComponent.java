package xyz.egor_d.artistsapp.presentation.artistinfo.di;

import dagger.Component;
import xyz.egor_d.artistsapp.presentation.PerActivity;
import xyz.egor_d.artistsapp.presentation.application.di.ApplicationComponent;
import xyz.egor_d.artistsapp.presentation.artistinfo.view.ArtistInfoFragment;

/**
 * A component for Activity with artist info. Modules for this component must be
 * scoped with {@link PerActivity} annotation
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = ArtistInfoModule.class
)
public interface ArtistInfoComponent {
    void inject(ArtistInfoFragment artistInfoFragment);
}

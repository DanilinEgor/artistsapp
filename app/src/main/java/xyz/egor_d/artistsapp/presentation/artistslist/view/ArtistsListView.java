package xyz.egor_d.artistsapp.presentation.artistslist.view;

import android.view.View;

import java.util.List;

import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.base.BaseView;

/**
 * An interface representing View in MVP
 */
public interface ArtistsListView extends BaseView {
    /**
     * Shows loading screen
     */
    void showLoading();

    /**
     * Hides loading screen
     */
    void hideLoading();

    /**
     * Shows artists list
     */
    void showData(List<Artist> artists);

    /**
     * Opens screen with artist info
     */
    void openInfo(Artist artist, View image);
}

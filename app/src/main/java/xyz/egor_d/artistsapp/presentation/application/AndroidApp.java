package xyz.egor_d.artistsapp.presentation.application;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;
import xyz.egor_d.artistsapp.BuildConfig;
import xyz.egor_d.artistsapp.presentation.application.di.ApplicationComponent;
import xyz.egor_d.artistsapp.presentation.application.di.ApplicationModule;
import xyz.egor_d.artistsapp.presentation.application.di.DaggerApplicationComponent;

/**
 * Application class for this app
 */
public class AndroidApp extends Application {
    protected ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        buildComponent();
        initLeakCanary();
        initTimber();
        initFabric();
    }

    /**
     * Init Fabric
     */
    private void initFabric() {
        if (!BuildConfig.DEBUG) {
            Fabric.with(this);
        }
    }

    /**
     * Inits {@link Timber} for debug
     */
    private void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    /**
     * Inits {@link LeakCanary} for debug mode
     */
    private void initLeakCanary() {
        if (BuildConfig.DEBUG) {
            LeakCanary.install(this);
        }
    }

    /**
     * Builds {@link ApplicationComponent}
     */
    protected void buildComponent() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}

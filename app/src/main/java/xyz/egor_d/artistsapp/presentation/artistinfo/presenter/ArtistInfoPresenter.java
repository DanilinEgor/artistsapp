package xyz.egor_d.artistsapp.presentation.artistinfo.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.artistsapp.domain.ArtistInfoInteractor;
import xyz.egor_d.artistsapp.presentation.Presenter;
import xyz.egor_d.artistsapp.presentation.artistinfo.view.ArtistInfoView;

/**
 * {@link Presenter} for {@link xyz.egor_d.artistsapp.presentation.artistinfo.view.ArtistInfoFragment}
 */
public class ArtistInfoPresenter implements Presenter {
    private final ArtistInfoInteractor artistInfoInteractor;
    private ArtistInfoView artistInfoView;
    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public ArtistInfoPresenter(ArtistInfoInteractor artistInfoInteractor) {
        this.artistInfoInteractor = artistInfoInteractor;
    }

    public ArtistInfoPresenter setArtistInfoView(final ArtistInfoView artistInfoView) {
        this.artistInfoView = artistInfoView;
        return this;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        subscription.add(
                artistInfoInteractor
                        .artist()
                        .subscribe(
                                artist -> artistInfoView.showInfo(artist),
                                throwable -> Timber.e(throwable, "Error: ")
                        )
        );
    }

    @Override
    public void onDestroyView() {

    }

    /**
     * Unsubscribe and unbind all views
     */
    @Override
    public void onDestroy() {
        subscription.unsubscribe();
        artistInfoView = null;
    }
}

package xyz.egor_d.artistsapp.presentation.artistinfo.view;

import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.base.BaseView;

/**
 * An interface representing View in MVP
 */
public interface ArtistInfoView extends BaseView {
    /**
     * Shows info about {@link Artist}
     *
     * @param artist artist for retrieving info
     */
    void showInfo(Artist artist);
}

package xyz.egor_d.artistsapp.presentation.artistslist.view;

import android.os.Bundle;

import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.presentation.HasComponent;
import xyz.egor_d.artistsapp.presentation.application.IMMLeaks;
import xyz.egor_d.artistsapp.presentation.artistslist.di.ArtistsListComponent;
import xyz.egor_d.artistsapp.presentation.artistslist.di.DaggerArtistsListComponent;
import xyz.egor_d.artistsapp.presentation.base.BaseActivity;

/**
 * Activity with artist info. Has {@link ArtistsListComponent}
 */
public class ArtistsListActivity extends BaseActivity implements HasComponent<ArtistsListComponent> {
    private ArtistsListComponent artistsListComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_fragment);

        buildComponent();

        if (savedInstanceState == null) {
            setFragment(new ArtistsListFragment());
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.artists);
        }

        // hack to avoid memory leak
        IMMLeaks.fixFocusedViewLeak(getApplication());
    }

    private void buildComponent() {
        artistsListComponent = DaggerArtistsListComponent
                .builder()
                .applicationComponent(getApplicationComponent())
                .build();
    }

    @Override
    public ArtistsListComponent getComponent() {
        return artistsListComponent;
    }
}

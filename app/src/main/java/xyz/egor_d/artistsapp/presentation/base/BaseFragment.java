package xyz.egor_d.artistsapp.presentation.base;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import xyz.egor_d.artistsapp.presentation.HasComponent;

/**
 * Basic fragment for all Fragments. Implements {@link BaseView}
 */
public class BaseFragment extends Fragment implements BaseView {
    /**
     * Gets Dagger component from Activity for this Fragment, Activity must implement
     * {@link HasComponent} interface
     *
     * @param componentType class of Component
     * @return implementation of Component
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }

    /**
     * Simply gets string from resources and shows it
     */
    @Override
    public void showErrorMessage(final int messageResId) {
        showErrorMessage(getString(messageResId));
    }

    /**
     * Basic implementation with only showing Toast for errors
     */
    @Override
    public void showErrorMessage(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}

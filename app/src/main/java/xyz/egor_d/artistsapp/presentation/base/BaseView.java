package xyz.egor_d.artistsapp.presentation.base;

/**
 * A basic interface for View in MVP. All Views should extend this interface
 */
public interface BaseView {
    /**
     * Shows error message from strings.xml with some id
     */
    void showErrorMessage(int messageResId);

    /**
     * Shows error message
     */
    void showErrorMessage(String message);
}

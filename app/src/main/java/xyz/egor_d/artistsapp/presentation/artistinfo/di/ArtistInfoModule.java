package xyz.egor_d.artistsapp.presentation.artistinfo.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import xyz.egor_d.artistsapp.data.repository.DataRepository;
import xyz.egor_d.artistsapp.domain.ArtistInfoInteractor;
import xyz.egor_d.artistsapp.presentation.PerActivity;

/**
 * Dagger module for {@link ArtistInfoComponent}
 */
@Module
public class ArtistInfoModule {
    private int artistId;

    public ArtistInfoModule(final int artistId) {
        this.artistId = artistId;
    }

    @PerActivity
    @Provides
    public ArtistInfoInteractor provideArtistInfoInteractor(
            @Named("subscribeScheduler") final Scheduler subscribeScheduler,
            @Named("observeScheduler") final Scheduler observeScheduler,
            DataRepository dataRepository
    ) {
        return new ArtistInfoInteractor(
                subscribeScheduler,
                observeScheduler,
                dataRepository,
                artistId
        );
    }
}

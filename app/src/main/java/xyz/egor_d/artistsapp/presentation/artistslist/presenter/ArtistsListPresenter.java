package xyz.egor_d.artistsapp.presentation.artistslist.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.data.exception.NoInternetConnectionException;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.domain.ArtistsListInteractor;
import xyz.egor_d.artistsapp.presentation.Presenter;
import xyz.egor_d.artistsapp.presentation.artistslist.view.ArtistsListView;

/**
 * {@link Presenter} for {@link xyz.egor_d.artistsapp.presentation.artistslist.view.ArtistsListFragment}
 */
public class ArtistsListPresenter implements Presenter {
    private final ArtistsListInteractor interactor;
    private ArtistsListView artistsListView;
    private CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public ArtistsListPresenter(ArtistsListInteractor interactor) {
        this.interactor = interactor;
    }

    public ArtistsListPresenter setArtistsListView(final ArtistsListView artistsListView) {
        this.artistsListView = artistsListView;
        return this;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            loadArtists(interactor.artists());
        }
    }

    @Override
    public void onCreate() {

    }

    /**
     * Unsubscribe and unbind all views
     */
    @Override
    public void onDestroy() {
        subscription.unsubscribe();
        artistsListView = null;
    }

    @Override
    public void onDestroyView() {

    }

    public void onArtistClicked(final Artist artist, View image) {
        artistsListView.openInfo(artist, image);
    }

    private void loadArtists(Observable<List<Artist>> dataObservable) {
        artistsListView.showLoading();
        subscription.add(
                dataObservable
                        .subscribe(
                                artists -> {
                                    artistsListView.showData(artists);
                                    artistsListView.hideLoading();
                                },
                                throwable -> {
                                    Timber.e(throwable, "Error: ");
                                    artistsListView.hideLoading();
                                    if (throwable instanceof NoInternetConnectionException) {
                                        artistsListView.showErrorMessage(R.string.exception_message_no_connection);
                                    } else {
                                        artistsListView.showErrorMessage(R.string.exception_message_other);
                                    }
                                },
                                () -> {
                                    artistsListView.hideLoading();
                                }
                        )
        );
    }

    public void onRefresh() {
        loadArtists(interactor.artistsFromCloud());
    }
}

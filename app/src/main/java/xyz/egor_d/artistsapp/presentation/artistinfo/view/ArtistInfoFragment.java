package xyz.egor_d.artistsapp.presentation.artistinfo.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.artistsapp.R;
import xyz.egor_d.artistsapp.data.model.Artist;
import xyz.egor_d.artistsapp.presentation.artistinfo.di.ArtistInfoComponent;
import xyz.egor_d.artistsapp.presentation.artistinfo.presenter.ArtistInfoPresenter;
import xyz.egor_d.artistsapp.presentation.base.BaseFragment;

/**
 * Fragment with artist info. Implements View in MVP
 */
public class ArtistInfoFragment extends BaseFragment implements ArtistInfoView {
    @Inject
    ArtistInfoPresenter artistInfoPresenter;
    @Inject
    Picasso picasso;

    @Bind(R.id.artist_info)
    TextView info;
    @Bind(R.id.artist_genres)
    TextView genres;
    @Bind(R.id.artist_bio)
    TextView bio;
    @Bind(R.id.artist_image)
    ImageView image;

    public ArtistInfoFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent(ArtistInfoComponent.class).inject(this);
        artistInfoPresenter.onCreate();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_artist_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        artistInfoPresenter.setArtistInfoView(this);
        artistInfoPresenter.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        artistInfoPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        artistInfoPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showInfo(final Artist artist) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (appCompatActivity.getSupportActionBar() != null) {
            appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
            appCompatActivity.getSupportActionBar().setTitle(artist.getName());
        }
        genres.setText(artist.getGenresString());
        info.setText(artist.getInfoString(getActivity()));
        bio.setText(artist.getBio());
        Linkify.addLinks(bio, Linkify.WEB_URLS);

        picasso
                .load(artist.getBigCoverUrl(artist.getCover()))
                .placeholder(R.drawable.placeholder_background)
                .error(R.drawable.placeholder_background)
                .into(image);
    }
}
